Technologies:

- electron
- webpack
- gulp

Installation:

- npm install
- gulp (if gulp is not acessible do 'npm install -g gulp' and/or 'npm install --save gulp')