var path = require("path");
module.exports = {
	entry: {
    app: ["./src/entry.js"]
 	 },
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/assets/",
    filename: "bundle.js"
  }
};