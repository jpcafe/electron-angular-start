const gulp = require('gulp');
const webpack = require('webpack');
const gutil = require('gulp-util');
const webpackConfig = require('./webpack.config.js');
const clean = require('gulp-clean');
const childProcess  = require('child_process');
const electron      = require('electron-prebuilt');
const rename = require('gulp-rename');

gulp.task('webpack', ['clean'], () => {
     // run webpack
    webpack(webpackConfig, function(err, stats) {
        if(err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
            // output options
        }));
    });
});

gulp.task('clean', () => {
    return gulp.src('dist', {read: false})
    .pipe(clean());
});

gulp.task('copy-primary', ['clean'], () => {
    gulp.src('./electron-primary/index.html')
    .pipe(gulp.dest('./dist'));

    gulp.src('./electron-primary/main.js')
    .pipe(gulp.dest('./dist'));

    return gulp.src('./electron-primary/package.json')
    .pipe(gulp.dest('./dist'));
});

gulp.task('run', ['build'], () => {
    childProcess.spawn(electron, ['dist'], { stdio: 'inherit' }); 
});

gulp.task('build', ['copy-primary', 'webpack']);

gulp.task('default', ['run'])